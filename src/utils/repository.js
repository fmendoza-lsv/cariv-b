import axios from "axios";

const repository = axios.create({
  baseURL: "http://localhost:3001",
});

export default repository;
