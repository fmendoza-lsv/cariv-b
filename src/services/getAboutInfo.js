import repository from "../utils/repository";

export const getAboutInfo = async () => {
  let res = await repository.get(`/about`);
  return res.data.info;
};

export default getAboutInfo;
