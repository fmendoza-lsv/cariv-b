import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route } from 'react-router-dom';
import { Footer } from './components/Footer/Footer';
import { Home } from './components/home/home';
import { StudentList } from './components/About/StudentList';
//import {IntroduccionGit} from './components/Menu/introduccionGit';
import { QuickstartGit } from "./components/Menu/quickstartGit";
import { IntroGitlab } from "./components/Menu/IntroGitlab";
import { ProfundizandoGit } from "./components/Menu/ProfundizandoGit";
import { Header } from "./components/Navbar/Header";
import { InstalacionGit } from "./components/Menu/InstalacionGit";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/instalacion-git" element={<InstalacionGit />} />
        <Route path="/" element={<Home />} />
        <Route path="/quickstart" element={<QuickstartGit />} />
        <Route path="/intro-gitlab" element={<IntroGitlab />} />
        <Route path="/profundizando" element={<ProfundizandoGit />} />
        <Route path="/about" element={<StudentList />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
