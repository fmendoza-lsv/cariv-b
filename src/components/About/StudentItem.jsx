export function StudentItem({
  id,
  nombre_completo,
  profesion,
  descripcion,
  correo,
}) {
  let imagenProfile = `../../src/assets/imgAbout/${id}.jpg`;
  return (

    <div className='container col-9 py-5'>
      <div
        className='card col-12 mb-2 text-dark'
        style={{
          height: '400px',
          background: 'white',
          boxShadow: '5px 5px 10px #9E9E9E',
        }}
      >
        {/*Le da las propiedades al cuadro exterior*/}

        <div className='d-flex col-6'>
          <img
            src={imagenProfile}
            // cambiar "imagen" por {foto}
            className='img-fluid rounded-start col-1'
            style={{
              marginLeft: '30px',
              marginTop: '-30px',
              height: '400px',
              width: '350px',
              objectFit: 'cover',
              boxShadow: '5px 5px 10px #9E9E9E',
            }}
            alt={nombre_completo}
          />
          {/*le da las propiedades a la foto*/}

          <div className='container col-12' >
            <div className='card-body'>
              <h5 className='card-title text-center'>{nombre_completo}</h5>
              <p className='text-muted text-center'> {profesion}</p>
              <p className='mb-0 text' style={{ textAlign: 'justify' }}>
                {descripcion}
              </p>
              <p className='card-footer col text-center'>
                {' '}
                <a href={correo}>{correo}</a>
              </p>
            </div>
          </div>
          {/*Le da las propiedades al texto*/}
        </div>
      </div>
    </div>
  );
}
