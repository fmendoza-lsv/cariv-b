import { useEffect, useState } from 'react';
import getComandsinfo from '../../services/getComandsInfo';
import {CommandList} from '../commands/commandList'

export function QuickstartGit() {

    const [comandInfo, setComandInfo] = useState(null);

    useEffect(() => {
        getComandsinfo().then(setComandInfo);
        console.log(comandInfo);
    }, []);

    return (
        <div className='container my-5 py-5'>
            <h1 className='text-center mb-3'>Quickstart en Git</h1>
            {
                comandInfo
                ? <CommandList comands={comandInfo}/>
                : <h2>Cargando</h2>
            }
        </div>
        
    );
}