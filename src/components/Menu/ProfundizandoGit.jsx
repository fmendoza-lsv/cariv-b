export function ProfundizandoGit() {
  return (
    <div className="container my-5 py-5">
      <h1 className="text-center mb-3">Profundizando en Git</h1>
      <h4 className="text-center mb-3">Conceptos y comandos</h4>
      <p>
        <strong>REFERENCIAS:</strong> Es un nombre que funciona como acceso
        directo, se asocian a un commit para facilitar el llamado a las rama; su
        nomenclatura es alfanumérica y por lo gral detalla ciertos aspectos como
        quien lo hizo, fecha, etc.,.
      </p>
      <p>
        <strong>HEAD:</strong> Es una referencia que apunta al commit mas
        reciente de la rama actual. Y se actualiza cada vez que se realice un
        nuevo commit o cada cambio de rama.
      </p>
      <p>
        <strong>TAGS(etiquetas):</strong> Son referencias estáticas y se
        utilizan como atajos a commits en específicos, y se diferencias de las
        ramas en que estos no cambian; y señalaran al mismo commit
      </p>
      <p>
        <strong>GIT LOG:</strong> Nos da un reporte de todas nuestras historias
        de commits y sus respectivas referencias, y nos ofrece variedad de
        opciones de comandos con lo cual se pueden realizar diferentes
        gestiones; ej: follow, grep, notes, author, etc..
      </p>
      <p>
        <strong>GIT SHOW:</strong> Nos permite mostrar objetos en formatos
        legibles, ej: nos muestra todas las operaciones que se realizaron en un
        commit
      </p>
      <p>
        <strong>GIT CHECKOUT:</strong> Nos sirve para movernos de una rama a la
        otra, para viajar en el tiempo entre commits, para crear branches etc.
      </p>
      <p>
        <strong>GIT STASH :</strong> Se utiliza cuando desee registrar el estado
        actual del directorio de trabajo y el índice, pero desee volver a un
        directorio de trabajo limpio. El comando guarda sus modificaciones
        locales y revierte el directorio de trabajo para que coincida con la
        Head confirmación.
      </p>
      <p id="item-3">
        <strong>GIT DIFF:</strong> Muestra los cambios entre el árbol de trabajo
        y el índice o un árbol, los cambios entre el índice y un árbol, los
        cambios entre dos árboles, los cambios resultantes de una combinación,
        los cambios entre dos objetos blob o los cambios entre dos archivos en
        el disco.
      </p>
      <p>
        <strong>GIT RESET:</strong> Restablece el HEAD actual al estado
        especificado
      </p>
    </div>
  );
}
