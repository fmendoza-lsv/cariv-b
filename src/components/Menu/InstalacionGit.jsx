import linux from "../../assets/images/ejemplo-linux.png";
import windows1 from "../../assets/images/windows1.png";
import windows2 from "../../assets/images/windows2.png";
export function InstalacionGit() {
  return (
    <div className="container my-5 py-5">
      <h1 className="text-center">Instalación de Git</h1>
      <ol>
        <li className="h4">Linux</li>
        <div className="pt-3">
          <p>
            <strong>➤ Apt:</strong>
          </p>
          <p>Los paquetes de Git estan disponibles mediante apt:</p>
          <p>1. Desde la terminal de comandos, escribimos lo siguiente:</p>
          <p className="mb-0 text-center">
            <strong>$ sudo apt-get update</strong>
          </p>
          <p className="text-center">
            <strong>$ sudo apt-get install git</strong>
          </p>
          <p className="text-justify">
            2. Para verificar que efectivamente se hizo la instalacion,
            ejecutamosla siguiente linea de comando el cual sirve para mostrar
            la version de git instalada en el equipo:
          </p>
          <p className="text-center">
            <strong>$ git --version </strong>
          </p>
          <p>
            <strong>➤ Fedora (dnf/yum):</strong>
          </p>
          <p>1. Desde la terminal de comandos, escribimos lo siguiente: </p>
          <p className="text-center">
            <strong>$ sudo dnf update</strong>
          </p>
          <p> Para las versionas mas antiguas de fedora: </p>
          <p className="text-center">
            <strong> $ sudo yum install git</strong>
          </p>
          <p className="text-justify">
            Para verificar que efectivamente se hizo la instalacion,
            ejecutamosla siguiente linea de comando el cual sirve para mostrar
            la version de git instalada en el equipo:
          </p>
          <p className="text-center">
            <strong>$ git --version</strong>
          </p>
          <p className="text-center">
            <img id="item-1-3-2" src={linux} alt="linux" />
          </p>
        </div>
        <li className="h4">Windows</li>
        <div className="pt-3">
          <p className="text-justify">
            Para instalar Git en Windows, debemos ingresar a la pagina de git:
            https://git-scm.com/downloads y escoger para descargar la version
            correspondiente al sistema operativo. Cuando se terine la descarga,
            ejecutamos el asistente de instalacion.
          </p>
          <p className="text-center">
            <img src={windows1} alt="windows" />
          </p>
          <p className="text-justfify">
            En la ventana de instalacion, seleccione las opciones Next y Finish
            para completar el proceso. Es recomendable utilizar las opciones
            predeterminadas a menos que necesites otra en especifico.
          </p>
          <p className="text-center">
            <img id="item-2" src={windows2} alt="windows" />
          </p>
        </div>
      </ol>
    </div>
  );
}
