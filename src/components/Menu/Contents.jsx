import logo from "../../assets/images/prueba.png";

export function Contents({ comandInfo }) {
  return (
    <div
      data-bs-spy="scroll"
      data-bs-target="#navbar-example3"
      data-bs-offset="0"
      tabIndex="0"
      id="item-1"
      className=""
    >
      <div className="container my-3">
        <h1 className="text-center mb-3">Introducción a Git</h1>
        <p className="text-center">
          <img
            src={logo}
            style={{
              //height: "300px",
              //width: "100%"
            }}
            alt="logo"
            id="item-1-1"
          />
        </p>
        <div className="ms-4">
          <h4 className="text-center mb-3">¿Qués es?</h4>
          <p className="text-justify">
            Git es un sistema de control de versiones distribuido. Esto
            significa que un clon local del proyecto es un repositorio de
            control de versiones completo. Estos repositorios locales plenamente
            funcionales permiten trabajar sin conexión o de forma remota
            fácilmente. Los desarrolladores confirman su trabajo localmente y, a
            continuación, sincronizan su copia del repositorio con la copia en
            el servidor.
          </p>
          <h4 className="text-center my-3">Características Principales</h4>
          Entre las características más importantes que Git nos brinda son:
          <div className="ms-3">
            <p className=" mt-3">
              ➤ Nos proporciona un listado de los archivos llamados Commits, con
              la fecha en que se modifica el archivo.
            </p>
            <p>
              ➤ Tener un control de versiones de los cambios realizados en los
              archivos de un proyecto.
            </p>
            <p>➤ Poder restablecer los cambios, volver atrás en el tiempo.</p>
            <p>➤ Crear Ramas o Branches.</p>
            <p id="item-1-3" className="mb-5">
              ➤ Realizar un mezclado de cambios entre los integrantes que llevan
              a cabo el Desarrollo del Proyecto.
            </p>
          </div>
        </div>
      </div>
      <div className="ms-4"></div>
    </div>
  );
}
