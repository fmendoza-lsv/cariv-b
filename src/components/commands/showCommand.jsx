import { Clipboard } from 'react-bootstrap-icons';
//import 'bootstrap-icons/fonts'

export function ShowCommand({ id, command }) {
  function copy(e, text) {
    
    if(e.target.classList.contains('clip')){
      navigator.clipboard.writeText(text).then(() => '');
    }
  }

  return (
    // <div class="d-flex justify-content-between align-items-center mb-3"> <span class="text-line me-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</span> <button onclick="copy('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod','#copy_button_1')" id="copy_button_1" class="btn btn-sm btn-success copy-button">Copy</button> </div>

    <div
      className='d-flex justify-content-between align-items-center alert alert-success alert-dismissible fade show align-middle'
      role='alert'
    >
      <div className='col-6 text-left copy'>{command}</div>
      <button
        className='btn btn-outline-success btn-copy clip'
        id={`copy_button_${id}`}
        onClick={(e) => copy(e, command)}
      >
        <Clipboard className='clip'/>
      </button>
    </div>
  );
}
