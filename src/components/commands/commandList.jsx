import {Command} from './command';

export function CommandList({comands}) {
    return (
    <div className="mt-5">
        
        {comands.map((comando) => {
            return <Command key={comando.id} {...comando}/>;
        })}
    </div>);
    
}

