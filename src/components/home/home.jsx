import { Contents } from '../Menu/Contents';

export function Home() {
  return (
    <div className='row mt-5'>
      <div className='p-3 mx-auto'>
        <Contents />
      </div>
    </div>
  );
}
