# CARIV B

## How to run the project?

To make it easy for you to run the project locally, here's a list of commands:

```
mkdir <folder_name>
cd <folder_name>
git remote add upstream https://gitlab.com/fmendoza-lsv/cariv-b.git
git pull upstream main
npm install
npm run start
```

Now, you can run locally the project in `http://localhost:3000/`.

Notice you must have the `npm` package installed in your system.
